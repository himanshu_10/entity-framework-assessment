﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class initialUpdatessss : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Orders_orderId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_orderId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "orderId",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "customerId",
                table: "Orders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_customerId",
                table: "Orders",
                column: "customerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Users_customerId",
                table: "Orders",
                column: "customerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Users_customerId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_customerId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "customerId",
                table: "Orders");

            migrationBuilder.AddColumn<int>(
                name: "orderId",
                table: "Users",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_orderId",
                table: "Users",
                column: "orderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Orders_orderId",
                table: "Users",
                column: "orderId",
                principalTable: "Orders",
                principalColumn: "Id");
        }
    }
}
