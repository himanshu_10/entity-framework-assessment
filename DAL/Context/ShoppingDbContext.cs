﻿using BO.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Context
{
    public class ShoppingDbContext : DbContext 
    {
        public ShoppingDbContext() { }

        public ShoppingDbContext(DbContextOptions<ShoppingDbContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }



        //Collection
        public DbSet<Role> Roles { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
            @"data source=INW-922\SQLEXPRESS;initial catalog=Assessment3;integrated security=true;TrustServerCertificate=True");
        }

    }
}
