﻿using BO.Models;
using DAL.Context;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Dal
    {

        ShoppingDbContext db = new ShoppingDbContext();
        //for admin

        // checking user type 
        public Boolean CheckUser(int id)
        { 
            User dobj = db.Users.Where(x => x.role.Id == id)
                                       .FirstOrDefault();
            if (dobj != null)
            {
                return true;
            }
            return false;
        }

        // getting list of roles ..
        public void ListOfRoles()
        { 
            List<Role> roles =  db.Roles.ToList();

            foreach (Role role in roles)
            {
                Console.WriteLine(role);
            }
            
        }


        public void ListSuppliers()
        {
            var query = from User in db.Users
                        join Role in db.Roles
                        on User.role.Id equals 3
                        select  new
                        {
                           SupplierId = User.Id, 
                           SupplierName = User.Name,
                           SupplierRole = User.role.RoleName 

                        };
            foreach (var order in query)
            {
                Console.WriteLine($"Supplier Id : {order.SupplierId}\t Supplier Name : {order.SupplierName}\t  Supplier Role : {order.SupplierRole}");
                //  Console.WriteLine(order);
            }

        }

        // add role 
        public void AddRole(int id, User user)
        {
            Role robj = db.Roles.Where(x => x.Id == id)
                                      .FirstOrDefault();
            if (robj != null)
            {
                foreach (Role temp in db.Roles)
                {
                    if (temp.Id == robj.Id)
                    {
                        user.role = temp;
                        //Sobj.Inventories.Add(inventory) ;
                    }
                }
            }
        }


        public void AddSupplier(int supId, Product product)
        {
            User uobj = db.Users.Where(x => x.Id == supId)
                                     .FirstOrDefault();
            if (uobj != null)
            {
                foreach (User temp in db.Users)
                {
                    if (temp.Id == uobj.Id)
                    {
                        product.supplier = temp;
                        //Sobj.Inventories.Add(inventory) ;
                    }
                }
            }
        }


        public int AddUser(User user)
        {
           
            db.Users.Add(user);
            db.SaveChanges();
            Console.WriteLine("User added!");
            return 0;
        }



        public int DeleteUser(int id)
        {
            User obj = db.Users.Where(x => x.Id == id).FirstOrDefault();
            if (obj != null)
            {
                obj.IsActive = false;
                Console.WriteLine("User deleted!");
                db.SaveChanges();
                return 0;
            }
            else
            {
                Console.WriteLine("No such user exists with this id");
                return 1;
            }



        }

        public User GetUserById(int id)
        {
            User u = db.Users.Find(id);
            if (u != null)
            {
                Console.WriteLine(u);
                return u;
            }
            else
            {
                Console.WriteLine("No Product Found");
                return null;
            }

        }


        //for supplier
        public int AddProduct(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
            Console.WriteLine("Product added!");
            return 0;
        }



        public int DeleteProduct(int id)
        {

            Product obj = db.Products.Where(x => x.Id == id).FirstOrDefault();
            if (obj != null)
            {
                db.Products.Remove(obj);
                db.SaveChanges();
                Console.WriteLine("Product deleted");
                return 0;
            }
            else
            {
                Console.WriteLine("No such product exists");
                return 1;
            }





        }



        public int UpdateProduct(Product product)
        {
            db.SaveChanges();
            Console.WriteLine("product updates");
            return 0;
        }



        //for customer



        public List<Product> GetProducts()
        {
            List<Product> list = db.Products.ToList();
            return list;
        }

        public Product GetProductsById(int id)
        {
            Product  p = db.Products.Find(id);
            if (p != null)
            {
                Console.WriteLine(p);
                return p;
            }
            else {
                Console.WriteLine("No Product Found");
                return null;
            }

        }

        public int PlaceOrder(Order order)
        {
            //List<Product> products = db.Products.ToList();
            //Console.WriteLine(products);
            //Console.WriteLine("select product id");
            //int prodId = Convert.ToInt32(Console.ReadLine());
            //List<User> users = db.Users.ToList();
            //Console.WriteLine(users);
            //Console.WriteLine("select user id");
            //int userId = Convert.ToInt32(Console.ReadLine());



            //Product ProductObj = db.Products.Find(prodId);
            //User UserObj = db.Users.Find(userId);



          //  order.Product = ProductObj;
            //order.User = UserObj;



            db.Orders.Add(order);
            db.SaveChanges();
            Console.WriteLine("Order has been placed!");
            return 0;



        }

        public void UpdateUser(User u)
        {
            db.SaveChanges();
        }

        public int UpdateProduct(string name, Product product)
        {
            Product obj = db.Products.Where(x => x.Product_Name == name).FirstOrDefault();
            if (obj != null)
            {
                if (product.Product_Name.Length > 0)
                    obj.Product_Name = product.Product_Name;
                if (product.Price > 0)
                    obj.Price = product.Price;

                if (product.Product_Description.Length > 0)
                    obj.Product_Description = product.Product_Description;

                if (product.QtyInStock > 0)
                    obj.QtyInStock = product.QtyInStock;



                db.Products.Update(obj);
                db.SaveChanges();
                return 0;
            }
            else
            {
                return 1;
            }




        }

        public void GetProducts(int id)
        {
            var u = from Order in db.Orders
                    join User in db.Users
                    on Order.customer.Id equals (id)
                    select new
                    {

                        UserName = User.Name,
                        UserRole = User.role.RoleName,
                        OrderName = Order.Product.Product_Name,
                        OrderDesc = Order.Product.Product_Description,
                        OrderPrice = Order.Product.Price

                    };

            foreach (var data in u)
            {
                Console.WriteLine($"UserName :{data.UserName}\t UserRole :{data.UserRole}\t OrderName :{data.OrderName}\t OrderDesc :{data.OrderDesc}\t OrderPirce :{data.OrderPrice} ");
            }
                    
                   

                    
        }
    }
}
