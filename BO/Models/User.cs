﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public Role role { get; set; } 

        public override string ToString()
        {
            return $"Id :{Id}\t User name :{Name}\t IsActive :{IsActive}";
        }

    }
}
