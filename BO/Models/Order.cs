﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Order
    {
        public int Id { get; set; }

        public DateTime ? PlacedOn { get; set; }

        public Product ? Product { get; set; }

        public User customer { get; set; }

        
    }
}
