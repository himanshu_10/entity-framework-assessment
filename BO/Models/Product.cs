﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Product
    {

        public int Id { get; set; }
        public string? Product_Name { get; set; }

        public string Product_Description { get; set; }

        public int QtyInStock { get; set; }

        public int Price { get; set; }

        public User? supplier { get; set; }

        //public Order? order { get; set; }

        public override string ToString()
        {
            return $"Id :{Id}\t ProductName :{Product_Name}\t ProductDescription :{Product_Description}\t Qty :{QtyInStock}\t Price :{Price}\t Supplier :{supplier}";
        }

    }
}
