﻿using BO.Models;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL
{
    public class Bal
    {

        Dal dal = new Dal();

        public Boolean CheckUser(int id)
        {
            return dal.CheckUser(id);

        }

        public void ListRoles()
        { 
           dal.ListOfRoles();
        }

        public void ListSuppliers()
        {
            dal.ListSuppliers();
        }

        // adding the Role
        public void AddRole(int roleId,User user)
        {
            dal.AddRole(roleId,user);
        }

        public int AddUser(User user)
        {
            return dal.AddUser(user);

        }

        public User GetUserById(int id)
        {
            return dal.GetUserById(id);
        }

        // adding the Role
        public void AddSupplier(int supId, Product product)
        {
            dal.AddSupplier(supId, product);
        }


        public int DeleteUser(int id)
        {
            return dal.DeleteUser(id);
        }



        public void AddProduct(Product product)
        {
             dal.AddProduct(product);
        }



        public int DeleteProduct(int id)
        {
            return dal.DeleteProduct(id);
        }



        public int UpdateProduct( Product product)
        {
            return dal.UpdateProduct(product);
        }



        public List<Product> GetProducts()
        {
            return dal.GetProducts();
        }



        public int PlaceOrder(Order order)
        {
            return dal.PlaceOrder(order);
        }

        public Product GetProductsById(int id)
        {
          return   dal.GetProductsById(id);
        }

        public void UpdateUser(User u)
        {
            dal.UpdateUser(u);
        }

        public void GetProducts(int id)
        {
             dal.GetProducts(id);
        }

        public int UpdateProduct(string? name, Product upDatedProduct)
        {
           return dal.UpdateProduct(name,upDatedProduct);
        }
    }
}
