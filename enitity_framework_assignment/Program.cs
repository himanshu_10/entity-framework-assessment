﻿
using BAL;
using BO.Models;

namespace entity_framework_assignment
{
    public class Program
    {

        public static void Main()
        {

            char ch = 'y';
            int option;
            Bal bal = new Bal();




            while (ch == 'y')
            {
                Console.WriteLine("  1.Admin");
                Console.WriteLine("  2.Customer");
                Console.WriteLine("  3.Supplier");



                option = Convert.ToInt32(Console.ReadLine());



                switch (option)
                {
                    case 1:
                        {
                            /*Console.WriteLine("Enter your id...");
                            int id = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Checking Admin or not......");*/

                         //   if (bal.CheckUser(id))
                         //   {
                                Console.WriteLine("  1.Add user");
                                Console.WriteLine("  2.Delete user");
                                int action = Convert.ToInt32(Console.ReadLine());

                                if (action == 1)
                                {
                                    Console.WriteLine("Enter Name : ");
                                    string uName = Console.ReadLine();

                                    Console.WriteLine("Select Roles..");
                                    bal.ListRoles();

                                    Console.WriteLine("\n Enter Role id..");
                                    int roleId = Convert.ToInt32(Console.ReadLine());
                                    User user = new User()
                                    {
                                        Name = uName,
                                        IsActive = true,
                                    };
                                    bal.AddRole(roleId, user);


                                    bal.AddUser(user);
                                }
                                else
                                {
                                    Console.WriteLine("enter user id");
                                    int uid = Convert.ToInt32(Console.ReadLine());
                                    bal.DeleteUser(uid);
                                }


                         //   }
                          /*  else
                            {

                                Console.WriteLine("You cannot perform the action  Not Admin....");
                            }*/

                        }
                        break;



                    case 2:
                        {
                           /* Console.WriteLine("Enter your id...");
                            int id = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Checking Customer or not......");*/

                          //  if (bal.CheckUser(id))
                           // {
                                Console.WriteLine("  1.List of Products");
                                Console.WriteLine("  2.Place order");
                                int type = Convert.ToInt32(Console.ReadLine());
                                if (type == 1)
                                {
                              //  Console.WriteLine("Enter User Id");
                              //  int id = Convert.ToInt32(Console.ReadLine()) ;
                                    List<Product> prod =    bal.GetProducts();
                                foreach(Product product in prod)
                                {
                                    Console.WriteLine(product);
                                }
                               //  bal.GetProducts(id);
                                

                            }
                                else
                                {
                                    Console.WriteLine("Enter User id:");
                                    int uid = Convert.ToInt32(Console.ReadLine());


                                    User u = bal.GetUserById(uid);

                                    if (u != null)
                                    {
                                        List<Product> products = bal.GetProducts();

                                        foreach (var x in products)
                                        {
                                            Console.WriteLine(x);
                                        }

                                        Console.WriteLine("enter product id : ");
                                        int pid = Convert.ToInt32(Console.ReadLine());
                                        Product p = bal.GetProductsById(pid);

                                        Order order
                                             = new Order
                                             {
                                                 PlacedOn = DateTime.Now,
                                                 Product = p,
                                                 customer = u
                                             };
                                          bal.PlaceOrder(order);
                                      
                                      //  bal.UpdateUser(u);
                                        
                                       
                                    }
                                }
                          //  }

                          /*  else {
                                Console.WriteLine("You are not customer...");
                            }*/

                        }

                        break;



                    case 3:
                        {
                            Console.WriteLine("  1.Add product");
                            Console.WriteLine("  2.Delete product");
                            Console.WriteLine("  3.Update product");
                          int  type = Convert.ToInt32(Console.ReadLine());



                            if (type == 1)
                            {
                                Console.WriteLine("Enter Product Name : ");
                                string pName = Console.ReadLine();

                                Console.WriteLine("Enter Product Details : ");
                                string pDetails = Console.ReadLine();

                                Console.WriteLine("Enter Qty in Stocks : ");
                                int pStock = Convert.ToInt32(Console.ReadLine());

                                Console.WriteLine("Enter Price : ");
                                int price = Convert.ToInt32(Console.ReadLine());

                                Console.WriteLine("Select Supplier..");
                                bal.ListSuppliers();

                                Console.WriteLine("\n Enter Supplier id..");
                                int supId = Convert.ToInt32(Console.ReadLine());

                                Product product = new Product()
                                {
                                    Product_Name = pName,
                                    Product_Description = pDetails,
                                    QtyInStock = pStock,
                                    Price = price
                                };
                                bal.AddSupplier(supId, product);
                                  bal.AddProduct(product);
                            }
                            else if (type == 2)
                            {
                                Console.WriteLine("enter product id");
                                int id = Convert.ToInt32(Console.ReadLine());
                                bal.DeleteProduct(id);
                            }
                            else
                            {

                                /* List<Product> products = bal.GetProducts();


                                 Console.WriteLine("Enter product Id you want to update");
                                 int id = Convert.ToInt32(Console.ReadLine());
                              Product p =    bal.GetProductsById(id);
                                 Console.WriteLine("Enter Product Name : ");
                                 string  pName = Console.ReadLine();

                                 Console.WriteLine("Enter Product Details : ");
                                 string  pDetails = Console.ReadLine();

                                 Console.WriteLine("Enter Qty in Stocks : ");
                                 int  pStock = Convert.ToInt32(Console.ReadLine());

                                 Console.WriteLine("Enter Price : ");
                                 int price = Convert.ToInt32(Console.ReadLine());

                                 Console.WriteLine("Select Supplier..");
                                 bal.ListSuppliers();

                                 Console.WriteLine("\n Enter Supplier id..");
                                 int  supId = Convert.ToInt32(Console.ReadLine());


                                     p.Product_Name = pName;
                                     p.Product_Description = pDetails;
                                     p.QtyInStock = pStock;
                                     p.Price = price;

                                 bal.AddSupplier(supId, p);
                                 bal.UpdateProduct( p);*/


                                Console.WriteLine("Enter the name of the product to edit");
                                string name = Console.ReadLine();


                                Console.WriteLine("Enter the new name");
                                string? ProductName = Console.ReadLine();

                                Console.WriteLine("Enter Product Details : ");
                                string ? pDetails = Console.ReadLine();

                                Console.WriteLine("Enter the new price");

                                int Price;
                                Int32.TryParse(Console.ReadLine(), out Price);
                                Console.WriteLine("Enter the new Quantity");
                                int Quantity;
                                Int32.TryParse(Console.ReadLine(), out Quantity);
                                Product upDatedProduct = new Product()
                                {
                                    Product_Name = ProductName,
                                    Price = Price,
                                    QtyInStock = Quantity,
                                    Product_Description = pDetails
                                    
                                };
                                int res = bal.UpdateProduct(name, upDatedProduct);
                                if (res == 0)
                                {
                                    Console.WriteLine("Product has been updated!");
                                }
                                else
                                {
                                    Console.WriteLine("It could not be updated");
                                }

                            }



                            break;



                        }


                    default:
                        Console.WriteLine("Enter a valid choice");
                        break;



                }



                Console.WriteLine("Do you want to repeat?");
                ch = Convert.ToChar(Console.ReadLine());
            }


        }
    }
}